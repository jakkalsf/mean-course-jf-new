import { Component, OnInit, OnDestroy } from '@angular/core';
import { Customer } from '../customer.model';
import { Subscription } from 'rxjs';
import { CustomersService } from '../customer.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit, OnDestroy {

  customers: Customer[] = [];
  private customersSub: Subscription;
  isLoading = false;

  constructor(public customersService: CustomersService) {}

  ngOnInit() {
    this.isLoading = true;
    this.customersService.getCustomers();
    this.customersSub = this.customersService.getCustomerUpdateListener()
      .subscribe((customers: Customer[]) => {
        this.customers = customers;
        this.isLoading = false;
      });
  }

  onDelete(customerId: string) {
    this.customersService.deleteCustomer(customerId);
  }

  ngOnDestroy() {
    this.customersSub.unsubscribe();
  }

}
