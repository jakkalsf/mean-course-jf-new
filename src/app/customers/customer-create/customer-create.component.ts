import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomersService } from '../customer.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.css']
})
export class CustomerCreateComponent implements OnInit {

  enteredName = '';
  enteredSurname = '';
  isLoading = false;
  private mode = 'create';
  private customerId: string;
  customer: Customer;

  constructor(public customersService: CustomersService, public route: ActivatedRoute) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('customerId')) {
        this.mode = 'edit';
        this.customerId = paramMap.get('customerId');
        this.isLoading = true;
        this.customersService.getCustomer(this.customerId).subscribe(customerData => {
          this.customer = customerData.customer;
          console.log(customerData.message);
          this.isLoading = false;
        });
      } else {
        this.mode = 'create';
        // this.mode = null;
      }
    });
  }

  onSaveCustomer(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    const customer: Customer = {
      id: null,
      name: form.value.name,
      surname: form.value.surname
    };
    if (this.mode === 'create') {
      this.customersService.addCustomer(form.value.name, form.value.surname);
    } else {
      this.customersService.updateCustomer(this.customerId, form.value.name, form.value.surname);
    }
    form.resetForm();
  }

}
