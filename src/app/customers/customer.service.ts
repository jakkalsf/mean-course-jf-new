import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Customer } from './customer.model';

@Injectable({providedIn: 'root'})
export class CustomersService {
  private customers: Customer[] = [];
  private customersUpdated = new Subject<Customer[]>();

  constructor(private http: HttpClient, private router: Router) {}

  getCustomers() {
    this.http.get<{ message: string, customers: any }>('http://localhost:3000/api/customers')
      .pipe(map(customerData => {
        return customerData.customers.map(customer => {
          return  {
            name: customer.name,
            surname: customer.surname,
            id: customer._id
          };
        });
      }))
      .subscribe(tansformedCustomers => {
        this.customers = tansformedCustomers;
        this.customersUpdated.next([...this.customers]);
      });
  }

  getCustomer(id: string) {
    return this.http.get<{message: string, customer: Customer}>('http://localhost:3000/api/customers/' + id);
  }

  getCustomerUpdateListener() {
    return this.customersUpdated.asObservable();
  }

  addCustomer(name: string, surname: string) {
    const customer: Customer = {id: null, name: name, surname: surname};
    this.http.post<{ message: string, customerId: string }>('http://localhost:3000/api/customers', customer)
      .subscribe(messageData => {
        customer.id = messageData.customerId;
        this.customers.push(customer);
        this.customersUpdated.next([...this.customers]);
        this.router.navigate(['/listcustomer']);
      });
  }

  updateCustomer(id: string, name: string, surname: string) {
    const customer: Customer = {id: id, name: name, surname: surname};
    this.http.put<{ message: string }>('http://localhost:3000/api/customers/' + id, customer)
      .subscribe(response => {
        const updatedCustomers = [...this.customers];
        const oldCustomerIndex = updatedCustomers.findIndex(p => p.id === id);
        updatedCustomers[oldCustomerIndex] = customer;
        this.customers = updatedCustomers;
        this.customersUpdated.next([...this.customers]);
        this.router.navigate(['/listcustomer']);
      });
  }

  deleteCustomer(customerId: string) {
    this.http.delete<{ message: string }>('http://localhost:3000/api/customers/' + customerId)
      .subscribe(() => {
        const updatedCustomers = this.customers.filter(customer => customer.id !== customerId);
        this.customers = updatedCustomers;
        this.customersUpdated.next([...this.customers]);
      });
  }
}
