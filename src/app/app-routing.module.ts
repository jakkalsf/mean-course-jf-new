import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostListComponent } from './posts/post-list/post-list.component';
import { PostCreateComponent } from './posts/post-create/post-create.component';
import { CustomerListComponent } from './customers/customer-list/customer-list.component';
import { CustomerCreateComponent } from './customers/customer-create/customer-create.component';

const routes: Routes = [
  // post routes
  { path: 'listpost', component: PostListComponent },
  { path: 'createpost', component: PostCreateComponent },
  { path: 'editpost/:postId', component: PostCreateComponent },
  // customer routes
  { path: 'listcustomer', component: CustomerListComponent },
  { path: 'createcustomer', component: CustomerCreateComponent },
  { path: 'editcustomer/:customerId', component: CustomerCreateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
