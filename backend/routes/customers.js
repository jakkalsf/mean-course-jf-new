const express = require('express');

const Customer = require('../models/customer');

const router = express.Router();

router.post('', (req, res, next) => {
  const customer = new Customer({
    name: req.body.name,
    surname: req.body.surname
  });
  customer.save().then(createdcustomer => {
    res.status(201).json({
      message: 'customer added successfully',
      customerId: createdcustomer._id
    });
  console.log('reached');
  });
});

router.put('/:id', (req, res, next) => {
  const customer = new Customer({
    _id: req.body.id,
    name: req.body.name,
    surname: req.body.surname
  });
  Customer.updateOne({_id: req.params.id}, customer).then(result => {
    res.status(200).json({message: 'Update successful!'});
  });
});

router.get('', (req, res, next) => {
  Customer.find()
    .then(documents => {
      res.status(200).json({
      message: 'customers fetched successfully!',
      customers: documents
    });
  });
});

router.get('/:id', (req, res, next) => {
  Customer.findById(req.params.id).then(customer => {
    if (customer) {
      res.status(200).json({message: 'customer found!', customer: {id: customer._id, name: customer.name, surname: customer.surname}});
    } else {
      res.status(404).json({message: 'customer not found!', customer: null});
    }
  });
});

router.delete('/:id', (req, res, next) => {
  Customer.deleteOne({_id: req.params.id})
    .then(result => {
      console.log(result);
      res.status(200).json({message: 'customer deleted!'});
    });
});

module.exports = router;
