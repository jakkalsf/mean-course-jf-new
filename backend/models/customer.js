const mongoose = require('mongoose');

const customerSchema = mongoose.Schema({
  name: { type: String, require: true },
  surname: { type: String, require: true },
});

module.exports = mongoose.model('Customer', customerSchema);
